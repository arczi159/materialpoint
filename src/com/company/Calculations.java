package com.company;

public class Calculations {
    static public Point2D positionGeometricCenter(Point2D[] point){
        Point2D point2D = new Point2D(0.0,0.0);
        for(Point2D pkt: point){
            point2D.setX(point2D.getX()+pkt.getX());
            point2D.setY(point2D.getY()+pkt.getY());
        }
        point2D.setX(point2D.getX()/2);
        point2D.setY(point2D.getY()/2);
        return point2D;
    }

    static public Point2D positionCenterOfMass(MaterialPoint2D[] materialPoint){
        MaterialPoint2D materialPoint2D = new MaterialPoint2D(1,1, 0.0);
        for(MaterialPoint2D pkt: materialPoint){
            materialPoint2D.setX(pkt.getX()*pkt.getMass());
            materialPoint2D.setY(pkt.getY()*pkt.getMass());
            materialPoint2D.setMass(materialPoint2D.getMass()+pkt.getMass());
        }
        materialPoint2D.setX(materialPoint2D.getX()/materialPoint2D.getMass());
        materialPoint2D.setY(materialPoint2D.getY()/materialPoint2D.getMass());
        return materialPoint2D;
    }


}
